# BasicAuthProxy

Is a simple proxy listening on localhost:PROXY_PORT which injects a Basic Auth Authorization Header using USERNAME and PASSWORD to calls and forwards them to the service configured at BASEURL.

##Installation
To setup the Proxy use the following steps:

1. clone this repo
2. npm install
3. configure for your usecase

##Configuration
It can be configured by supplying the following ENV variables:

* USERNAME: username to use for basic auth.
* PASSWORD: password to use for basic auth.
* BASEURL: baseurl of the service this proxy forwards to.
* PROXY_PORT: port proxy is listening to.

You can also configure it in the "index.js"

##Start
To start the proxy either use: 

* node index.js
* npm start
* ./startProxy.sh