const express = require('express')
const proxy = require('http-proxy-middleware')

const app = express()

// configuration: either use ENV variables or configure here
const USERNAME = process.env.USERNAME || '<username>'
const PASSWORD = process.env.PASSWORD || '<password>'
const BASEURL = process.env.BASEURL || 'https://example.org'
const PORT = process.env.PROXY_PORT || 4321

app.use(proxy({target: BASEURL, secure: false, onProxyReq: (proxyReq, req, res) => {
  proxyReq.setHeader('Authorization', `Basic ${new Buffer(`${USERNAME}:${PASSWORD}`).toString('base64')}`)
}}))

app.listen(PORT, () => {
  console.log(`Basic Auth Proxy: forwarding "http://localhost:${PORT}" to "${BASEURL}" with user credentials "${USERNAME}"`)
})